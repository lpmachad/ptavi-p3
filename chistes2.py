#!/usr/bin/python3
# -*- coding: utf-8 -*-

import xml.dom.minidom


def main():
    """Programa principal"""
    document = xml.dom.minidom.parse('chistes.xml')
    jokes = document.getElementsByTagName('chiste')
    for joke in jokes[::-1]:    # El bucle al reves
        score = joke.getAttribute('calificacion')
        questions = joke.getElementsByTagName('pregunta')
        question = questions[0].firstChild.nodeValue.strip()
        answers = joke.getElementsByTagName('respuesta')
        answer = answers[0].firstChild.nodeValue.strip()
        print(f"Calificación: {score}.")    # Primero imprimios la calificacion
        print(f" Respuesta: {answer}")    # Segundo imprimimos la respuesta
        print(f" Pregunta: {question}")    # Tercero imprimios la pregunta


if __name__ == "__main__":
    main()
