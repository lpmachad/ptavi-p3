#!/usr/bin/python3
# -*- coding: utf-8 -*-

import xml.dom.minidom


def main(path):
    """Programa principal"""
    obj = Humor(path)    # Introducimos el fichero en la clase Humor
    for joke in obj.jokes():    # Bucle que itera sobre chistes dentro de Humor
        print(f"Calificación: {joke['score']}.")    # Imprime el valor de calificacion
        print(f" Respuesta: {(joke['answer'])}")    # Imprime el valor de respuesta
        print(f" Pregunta: {(joke['question'])}\n")    # Imprime el valor de pregunta


class Humor:    # Clase Humor
    def __init__(self, path):
        self.path = path    # Nombre del fichero

    def jokes(self):
        calificacion = ['buenisimo', 'bueno', 'regular', 'malo', 'malisimo']
        document = xml.dom.minidom.parse(self.path)    # Fichero
        if document.getElementsByTagName('humor'):    # Condicion Humor
            jokes = document.getElementsByTagName('chiste')
            chistes = []
            result = []    # Devuelve la lista de chistes

            for joke in jokes:
                score = joke.getAttribute('calificacion')
                questions = joke.getElementsByTagName('pregunta')
                question = questions[0].firstChild.nodeValue.strip()
                answers = joke.getElementsByTagName('respuesta')
                answer = answers[0].firstChild.nodeValue.strip()
                chistes.append((score, question, answer))

            for calif in calificacion:
                for chiste in chistes:
                    chiste_calif = chiste[0]    # Nos quedamos con el primer elemento de chiste, la calificacion
                    if chiste_calif == calif:    # Si la califiacion es igual al valor de score
                        result.append({'score': chiste_calif, 'answer': chiste[2], 'question': chiste[1]})
                        # Añade en la lista result la calificacion, pregunta y respuesta
            return result
        else:
            raise Exception("Root element is not humor")    # Levanta la expecion si no pertener al root 'Humor'


if __name__ == "__main__":
    main('tests/chistes8.xml')    # El fichero se encuentra en test
