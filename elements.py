#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import smil  # para poder trabajar con el documento karaoke


def main(names):
    """Programa principal"""
    filename = str(names)  # convertimos a cadena de texto

    file = smil.SMIL(filename)  # lee el archivo smil utilizando la biblioteca SMIL
    for element in file.elements():  # itera por el archivo karaoke por cada elemento
        print(element.name())  # luego imprime el nombre


if __name__ == "__main__":
    if len(sys.argv) != 2:
        sys.exit("Usage: python3 elements.py <file>")  # excepcion de ayuda
    main(sys.argv[1])
