#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Simple program to parse a chistes XML file"""

import xml.dom.minidom


def main():
    """Programa principal"""
    document = xml.dom.minidom.parse('chistes.xml')
    # Crear el archivo dom de chistes.xml
    jokes = document.getElementsByTagName('chiste')
    # jokes coge de document y los elemento
    # con la etiqueta chiste hasta /chiste
    for joke in jokes:   # for n in jokes itera
        score = joke.getAttribute('calificacion')
        # Guarda el atributo de cada etiqueta (malo, bueno, malisimo...)
        questions = joke.getElementsByTagName('pregunta')
        # Guarda las preguntas
#       print(questions[0].firstChild.nodeValue)
        # Imprime los elementos de la lista question uno a uno
        question = questions[0].firstChild.nodeValue.strip()
        # First child saca lo que tiene dentro, el hijo,
        # te lo pasa a un strig y te quita los espacios
        answers = joke.getElementsByTagName('respuesta')
        # Guarda las respuestas
        answer = answers[0].firstChild.nodeValue.strip()
        # Saca el primer elemento de answer, el hijo,
        # lo pasa a string y lo separa
        print(f"Calificación: {score}.")
        # {lo que esta dentro de los corchetes es el valor de score}
        print(f" Pregunta: {question}")
        print(f" Respuesta: {answer}")


if __name__ == "__main__":
    main()
