#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import smil
import json


class SMILJSON(smil.SMIL):
    def json(self):
        lista = []  # lista vacia para devolver los datos del documento karaoke

        for element in self.elements():  # iterar por todos los elementos del documento
            element_obj = {
                'name': element.name(),
                'attrs': element.attrs()
            }
            lista.append(element_obj)  # añadir elementos a la lista

        return json.dumps(lista, indent=2)  # json.dump convierte la lista en formato JSON
        # indent=2 sangria

# no ha sido necesaria la clase ElementJSON

def main(path):
    """Programa principal"""
    a = SMILJSON(path)
    print(a.json())


if __name__ == "__main__":
    if len(sys.argv) != 2:
        sys.exit("Usage: python3 elements.py <file>")  # excepcion de ayuda
    main(sys.argv[1])
