#!/usr/bin/python3
# -*- coding: utf-8 -*-

import xml.dom.minidom
import xml.etree.ElementTree
# Importamos un nuevo modulo xml para usar el arbol entero


class Element:    # Clase Element
    def __init__(self, name, attrs):
        self.element_name = name
        # element_name es el nombre
        self.attr_dict = attrs
        # attr_direct es el atributo

    def name(self):
        return self.element_name
        # Devuelve el nombre del elemento

    def attrs(self):
        return self.attr_dict
        # Devuelve los atributos del diccionario


class SMIL:    # Clase SMIL
    def __init__(self, path):
        self.path = path
        # path es el nombre del fichero para analizar

    def elements(self):
        document = xml.etree.ElementTree.parse(self.path)
        # Crear el arbol del path, que es el archivo
        elements = list()
        # Lista vacia elements
        elemen_type = list()
        # Liste vacia elementTypeList

        for elem in document.iter():
            # Iteramos dentro del document (path)
            elements.append(elem)
            # Añade los elementos dentro del document

        for element in elements:
            # Iteramos dentro de la lista anterior, elements
            elemen_type.append(Element(element.tag, element.attrib))
            # Crear elementos de tipo elemento
            # con el nombre y el atributo

        return elemen_type


if __name__ == '__main__':
    obj = SMIL('karaoke.smil')
    x = obj.elements()
