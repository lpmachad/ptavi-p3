#!/usr/bin/python3
# -*- coding: utf-8 -*-

import xml.dom.minidom


def main(path):    # Argumento de la funcion
    """Programa principal"""
    document = xml.dom.minidom.parse(path)
    calificaciones = ['buenisimo', 'bueno', 'regular', 'malo', 'malisimo']  # Creamos una lista
    if document.getElementsByTagName('humor'):    # Condicion si el elemento raiz se llama humor
        jokes = document.getElementsByTagName('chiste')
        chistes = []    # Creamos una lista vacía donde se guardarán los chistes
        # con su calificación y su pregunta/respuesta
        for joke in jokes:
            score = joke.getAttribute('calificacion')    # Calificacion tiene atributo
            questions = joke.getElementsByTagName('pregunta')    # Pregunta tiene el contenido de la etiqueta
            question = questions[0].firstChild.nodeValue.strip()
            answers = joke.getElementsByTagName('respuesta')
            answer = answers[0].firstChild.nodeValue.strip()
            chistes.append((score, question, answer))

        for calificacion in calificaciones:    # Bucle que itera sobre la lista de los tipos de calificacion
            for chiste in chistes:    # Bucle que itera sobre chistes, que estan en el archivo xml
                joke_score = chiste[0]
                if joke_score == calificacion:    # Si el valor de calificacion es igual al score de los chistes
                    print(f"Calificación: {calificacion}.")    # Imprime el primer valor score
                    print(f" Respuesta: {(chiste[2])}")    # Imprime el segundo valor respuesta
                    print(f" Pregunta: {(chiste[1])}\n")  # Imprime el tercer valor pregunta
                else:
                    pass
    else:
        raise Exception("Root element is not humor")    # Si no cumple la condicion el elemento raiz
        # levanta este mensaje


if __name__ == "__main__":
    main('tests/chistes8.xml')    # esto es path
